<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Featured Property Field -->
<div class="form-group col-sm-6">
    {!! Form::label('featured_property', 'Featured Property:') !!}
    {!! Form::number('featured_property', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_name', 'Property Name:') !!}
    {!! Form::text('property_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_slug', 'Property Slug:') !!}
    {!! Form::text('property_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_type', 'Property Type:') !!}
    {!! Form::text('property_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Purpose Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_purpose', 'Property Purpose:') !!}
    {!! Form::text('property_purpose', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    {!! Form::text('sale_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Rent Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rent_price', 'Rent Price:') !!}
    {!! Form::text('rent_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Map Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('map_latitude', 'Map Latitude:') !!}
    {!! Form::text('map_latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Map Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('map_longitude', 'Map Longitude:') !!}
    {!! Form::text('map_longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Bathrooms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bathrooms', 'Bathrooms:') !!}
    {!! Form::text('bathrooms', null, ['class' => 'form-control']) !!}
</div>

<!-- Bedrooms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bedrooms', 'Bedrooms:') !!}
    {!! Form::text('bedrooms', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Featured Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('featured_image', 'Featured Image:') !!}
    {!! Form::text('featured_image', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Images1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_images1', 'Property Images1:') !!}
    {!! Form::text('property_images1', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Images2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_images2', 'Property Images2:') !!}
    {!! Form::text('property_images2', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Images3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_images3', 'Property Images3:') !!}
    {!! Form::text('property_images3', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Images4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_images4', 'Property Images4:') !!}
    {!! Form::text('property_images4', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Images5 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_images5', 'Property Images5:') !!}
    {!! Form::text('property_images5', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('properties.index') !!}" class="btn btn-default">Cancel</a>
</div>
