<div class="table-responsive">
    <table class="table" id="properties-table">
        <thead>
            <tr>
                <th>User Id</th>
        <th>Featured Property</th>
        <th>Property Name</th>
        <th>Property Slug</th>
        <th>Property Type</th>
        <th>Property Purpose</th>
        <th>Sale Price</th>
        <th>Rent Price</th>
        <th>Address</th>
        <th>Map Latitude</th>
        <th>Map Longitude</th>
        <th>Bathrooms</th>
        <th>Bedrooms</th>
        <th>Area</th>
        <th>Description</th>
        <th>Featured Image</th>
        <th>Property Images1</th>
        <th>Property Images2</th>
        <th>Property Images3</th>
        <th>Property Images4</th>
        <th>Property Images5</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($properties as $property)
            <tr>
                <td>{!! $property->user_id !!}</td>
            <td>{!! $property->featured_property !!}</td>
            <td>{!! $property->property_name !!}</td>
            <td>{!! $property->property_slug !!}</td>
            <td>{!! $property->property_type !!}</td>
            <td>{!! $property->property_purpose !!}</td>
            <td>{!! $property->sale_price !!}</td>
            <td>{!! $property->rent_price !!}</td>
            <td>{!! $property->address !!}</td>
            <td>{!! $property->map_latitude !!}</td>
            <td>{!! $property->map_longitude !!}</td>
            <td>{!! $property->bathrooms !!}</td>
            <td>{!! $property->bedrooms !!}</td>
            <td>{!! $property->area !!}</td>
            <td>{!! $property->description !!}</td>
            <td>{!! $property->featured_image !!}</td>
            <td>{!! $property->property_images1 !!}</td>
            <td>{!! $property->property_images2 !!}</td>
            <td>{!! $property->property_images3 !!}</td>
            <td>{!! $property->property_images4 !!}</td>
            <td>{!! $property->property_images5 !!}</td>
            <td>{!! $property->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['properties.destroy', $property->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('properties.show', [$property->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('properties.edit', [$property->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
