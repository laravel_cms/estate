<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $property->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $property->user_id !!}</p>
</div>

<!-- Featured Property Field -->
<div class="form-group">
    {!! Form::label('featured_property', 'Featured Property:') !!}
    <p>{!! $property->featured_property !!}</p>
</div>

<!-- Property Name Field -->
<div class="form-group">
    {!! Form::label('property_name', 'Property Name:') !!}
    <p>{!! $property->property_name !!}</p>
</div>

<!-- Property Slug Field -->
<div class="form-group">
    {!! Form::label('property_slug', 'Property Slug:') !!}
    <p>{!! $property->property_slug !!}</p>
</div>

<!-- Property Type Field -->
<div class="form-group">
    {!! Form::label('property_type', 'Property Type:') !!}
    <p>{!! $property->property_type !!}</p>
</div>

<!-- Property Purpose Field -->
<div class="form-group">
    {!! Form::label('property_purpose', 'Property Purpose:') !!}
    <p>{!! $property->property_purpose !!}</p>
</div>

<!-- Sale Price Field -->
<div class="form-group">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    <p>{!! $property->sale_price !!}</p>
</div>

<!-- Rent Price Field -->
<div class="form-group">
    {!! Form::label('rent_price', 'Rent Price:') !!}
    <p>{!! $property->rent_price !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $property->address !!}</p>
</div>

<!-- Map Latitude Field -->
<div class="form-group">
    {!! Form::label('map_latitude', 'Map Latitude:') !!}
    <p>{!! $property->map_latitude !!}</p>
</div>

<!-- Map Longitude Field -->
<div class="form-group">
    {!! Form::label('map_longitude', 'Map Longitude:') !!}
    <p>{!! $property->map_longitude !!}</p>
</div>

<!-- Bathrooms Field -->
<div class="form-group">
    {!! Form::label('bathrooms', 'Bathrooms:') !!}
    <p>{!! $property->bathrooms !!}</p>
</div>

<!-- Bedrooms Field -->
<div class="form-group">
    {!! Form::label('bedrooms', 'Bedrooms:') !!}
    <p>{!! $property->bedrooms !!}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{!! $property->area !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $property->description !!}</p>
</div>

<!-- Featured Image Field -->
<div class="form-group">
    {!! Form::label('featured_image', 'Featured Image:') !!}
    <p>{!! $property->featured_image !!}</p>
</div>

<!-- Property Images1 Field -->
<div class="form-group">
    {!! Form::label('property_images1', 'Property Images1:') !!}
    <p>{!! $property->property_images1 !!}</p>
</div>

<!-- Property Images2 Field -->
<div class="form-group">
    {!! Form::label('property_images2', 'Property Images2:') !!}
    <p>{!! $property->property_images2 !!}</p>
</div>

<!-- Property Images3 Field -->
<div class="form-group">
    {!! Form::label('property_images3', 'Property Images3:') !!}
    <p>{!! $property->property_images3 !!}</p>
</div>

<!-- Property Images4 Field -->
<div class="form-group">
    {!! Form::label('property_images4', 'Property Images4:') !!}
    <p>{!! $property->property_images4 !!}</p>
</div>

<!-- Property Images5 Field -->
<div class="form-group">
    {!! Form::label('property_images5', 'Property Images5:') !!}
    <p>{!! $property->property_images5 !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $property->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $property->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $property->updated_at !!}</p>
</div>

