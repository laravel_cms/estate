<li class="{{ Request::is('properties*') ? 'active' : '' }}">
    <a href="{!! route('properties.index') !!}"><i class="fa fa-edit"></i><span>Properties</span></a>
</li>


<li class="{{ Request::is('partners*') ? 'active' : '' }}">
    <a href="{!! route('partners.index') !!}"><i class="fa fa-edit"></i><span>Partners</span></a>
</li>


<li class="{{ Request::is('settings*') ? 'active' : '' }}">
    <a href="{!! route('settings.index') !!}"><i class="fa fa-edit"></i><span>Settings</span></a>
</li>

<li class="{{ Request::is('testimonials*') ? 'active' : '' }}">
    <a href="{!! route('testimonials.index') !!}"><i class="fa fa-edit"></i><span>Testimonials</span></a>
</li>

