  <div class="widget sidebar-widget featured-properties-widget">
                        <h3 class="widgettitle">Featured Properties</h3>
                        <ul class="owl-carousel owl-alt-controls1 single-carousel owl-theme" data-columns="1" data-autoplay="no" data-pagination="no" data-arrows="yes" data-single-item="yes" style="opacity: 1; display: block;">
                          <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 960px; left: 0px; display: block;"><div class="owl-item" style="width: 240px;"><li class="item property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:0px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Rent</span> </a>
                            <div class="property-info">
                              <h4><a href="#">116 Waverly Place</a></h4>
                              <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                              <div class="price"><strong>$</strong><span>2800 Monthly</span></div>
                            </div>
                          </li></div><div class="owl-item" style="width: 240px;"><li class="item property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:0px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Buy</span> </a>
                            <div class="property-info">
                              <h4><a href="#">232 East 63rd Street</a></h4>
                              <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                              <div class="price"><strong>$</strong><span>250000</span></div>
                            </div>
                          </li></div><div class="owl-item" style="width: 240px;"><li class="item property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:0px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Buy</span> </a>
                            <div class="property-info">
                              <h4><a href="#">55 Warren Street</a></h4>
                              <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                              <div class="price"><strong>$</strong><span>300000</span></div>
                            </div>
                          </li></div><div class="owl-item" style="width: 240px;"><li class="item property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:0px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Rent</span> </a>
                            <div class="property-info">
                              <h4><a href="#">459 West Broadway</a></h4>
                              <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                              <div class="price"><strong>$</strong><span>3100 Monthly</span></div>
                            </div>
                          </li></div></div></div>
                          
                          
                          
                        <div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-chevron-left"></i></div><div class="owl-next"><i class="fa fa-chevron-right"></i></div></div></div></ul>
                    </div>