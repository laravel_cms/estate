<div class="widget sidebar-widget">
  <h3 class="widgettitle">Search Properties</h3>
  <div class="full-search-form">

      <form action="{{route('searchproperties')}}" method="post">
       @csrf
          <select name="type" class="form-control input-lg selectpicker">
              <option selected>Type</option>
            <option>Villa</option>
            <option>Family House</option>
            <option>Single Home</option>
            <option>Cottage</option>
            <option>Apartment</option>
          </select>
          <select name="purpose" class="form-control input-lg selectpicker">
              <option selected>Contract</option>
            <option>Rent</option>
            <option>Buy</option>
          </select>
          <select name="location" class="form-control input-lg selectpicker">
              <option selected>Location</option>
            <option>New York</option>
          </select>
          <input type="text" name="min_price" class="form-control">
          <input type="text" name="max_price" class="form-control">
        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
    </form>

  </div>
</div>