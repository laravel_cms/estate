<div class="table-responsive">
    <table class="table" id="settings-table">
        <thead>
            <tr>
                <th>Site Style</th>
        <th>Site Name</th>
        <th>Site Email</th>
        <th>Site Logo</th>
        <th>Site Favicon</th>
        <th>Site Description</th>
        <th>Site Header Code</th>
        <th>Site Footer Code</th>
        <th>Site Copyright</th>
        <th>Footer Widget1</th>
        <th>Footer Widget2</th>
        <th>Footer Widget3</th>
        <th>Addthis Share Code</th>
        <th>Disqus Comment Code</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($settings as $setting)
            <tr>
                <td>{!! $setting->site_style !!}</td>
            <td>{!! $setting->site_name !!}</td>
            <td>{!! $setting->site_email !!}</td>
            <td>{!! $setting->site_logo !!}</td>
            <td>{!! $setting->site_favicon !!}</td>
            <td>{!! $setting->site_description !!}</td>
            <td>{!! $setting->site_header_code !!}</td>
            <td>{!! $setting->site_footer_code !!}</td>
            <td>{!! $setting->site_copyright !!}</td>
            <td>{!! $setting->footer_widget1 !!}</td>
            <td>{!! $setting->footer_widget2 !!}</td>
            <td>{!! $setting->footer_widget3 !!}</td>
            <td>{!! $setting->addthis_share_code !!}</td>
            <td>{!! $setting->disqus_comment_code !!}</td>
                <td>
                    {!! Form::open(['route' => ['settings.destroy', $setting->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('settings.show', [$setting->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('settings.edit', [$setting->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
