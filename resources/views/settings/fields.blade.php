<!-- Site Style Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_style', 'Site Style:') !!}
    {!! Form::text('site_style', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_name', 'Site Name:') !!}
    {!! Form::text('site_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_email', 'Site Email:') !!}
    {!! Form::text('site_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_logo', 'Site Logo:') !!}
    {!! Form::text('site_logo', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Favicon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_favicon', 'Site Favicon:') !!}
    {!! Form::text('site_favicon', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_description', 'Site Description:') !!}
    {!! Form::text('site_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Header Code Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('site_header_code', 'Site Header Code:') !!}
    {!! Form::textarea('site_header_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Footer Code Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('site_footer_code', 'Site Footer Code:') !!}
    {!! Form::textarea('site_footer_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Site Copyright Field -->
<div class="form-group col-sm-6">
    {!! Form::label('site_copyright', 'Site Copyright:') !!}
    {!! Form::text('site_copyright', null, ['class' => 'form-control']) !!}
</div>

<!-- Footer Widget1 Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('footer_widget1', 'Footer Widget1:') !!}
    {!! Form::textarea('footer_widget1', null, ['class' => 'form-control']) !!}
</div>

<!-- Footer Widget2 Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('footer_widget2', 'Footer Widget2:') !!}
    {!! Form::textarea('footer_widget2', null, ['class' => 'form-control']) !!}
</div>

<!-- Footer Widget3 Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('footer_widget3', 'Footer Widget3:') !!}
    {!! Form::textarea('footer_widget3', null, ['class' => 'form-control']) !!}
</div>

<!-- Addthis Share Code Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('addthis_share_code', 'Addthis Share Code:') !!}
    {!! Form::textarea('addthis_share_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Disqus Comment Code Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('disqus_comment_code', 'Disqus Comment Code:') !!}
    {!! Form::textarea('disqus_comment_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('settings.index') !!}" class="btn btn-default">Cancel</a>
</div>
