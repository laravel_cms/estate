<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $setting->id !!}</p>
</div>

<!-- Site Style Field -->
<div class="form-group">
    {!! Form::label('site_style', 'Site Style:') !!}
    <p>{!! $setting->site_style !!}</p>
</div>

<!-- Site Name Field -->
<div class="form-group">
    {!! Form::label('site_name', 'Site Name:') !!}
    <p>{!! $setting->site_name !!}</p>
</div>

<!-- Site Email Field -->
<div class="form-group">
    {!! Form::label('site_email', 'Site Email:') !!}
    <p>{!! $setting->site_email !!}</p>
</div>

<!-- Site Logo Field -->
<div class="form-group">
    {!! Form::label('site_logo', 'Site Logo:') !!}
    <p>{!! $setting->site_logo !!}</p>
</div>

<!-- Site Favicon Field -->
<div class="form-group">
    {!! Form::label('site_favicon', 'Site Favicon:') !!}
    <p>{!! $setting->site_favicon !!}</p>
</div>

<!-- Site Description Field -->
<div class="form-group">
    {!! Form::label('site_description', 'Site Description:') !!}
    <p>{!! $setting->site_description !!}</p>
</div>

<!-- Site Header Code Field -->
<div class="form-group">
    {!! Form::label('site_header_code', 'Site Header Code:') !!}
    <p>{!! $setting->site_header_code !!}</p>
</div>

<!-- Site Footer Code Field -->
<div class="form-group">
    {!! Form::label('site_footer_code', 'Site Footer Code:') !!}
    <p>{!! $setting->site_footer_code !!}</p>
</div>

<!-- Site Copyright Field -->
<div class="form-group">
    {!! Form::label('site_copyright', 'Site Copyright:') !!}
    <p>{!! $setting->site_copyright !!}</p>
</div>

<!-- Footer Widget1 Field -->
<div class="form-group">
    {!! Form::label('footer_widget1', 'Footer Widget1:') !!}
    <p>{!! $setting->footer_widget1 !!}</p>
</div>

<!-- Footer Widget2 Field -->
<div class="form-group">
    {!! Form::label('footer_widget2', 'Footer Widget2:') !!}
    <p>{!! $setting->footer_widget2 !!}</p>
</div>

<!-- Footer Widget3 Field -->
<div class="form-group">
    {!! Form::label('footer_widget3', 'Footer Widget3:') !!}
    <p>{!! $setting->footer_widget3 !!}</p>
</div>

<!-- Addthis Share Code Field -->
<div class="form-group">
    {!! Form::label('addthis_share_code', 'Addthis Share Code:') !!}
    <p>{!! $setting->addthis_share_code !!}</p>
</div>

<!-- Disqus Comment Code Field -->
<div class="form-group">
    {!! Form::label('disqus_comment_code', 'Disqus Comment Code:') !!}
    <p>{!! $setting->disqus_comment_code !!}</p>
</div>

