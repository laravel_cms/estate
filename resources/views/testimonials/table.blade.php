<div class="table-responsive">
    <table class="table" id="testimonials-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Testimonial</th>
        <th>T User Image</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($testimonials as $testimonial)
            <tr>
                <td>{!! $testimonial->name !!}</td>
            <td>{!! $testimonial->testimonial !!}</td>
            <td>{!! $testimonial->t_user_image !!}</td>
                <td>
                    {!! Form::open(['route' => ['testimonials.destroy', $testimonial->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('testimonials.show', [$testimonial->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('testimonials.edit', [$testimonial->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
