<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Testimonial Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('testimonial', 'Testimonial:') !!}
    {!! Form::textarea('testimonial', null, ['class' => 'form-control']) !!}
</div>

<!-- T User Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('t_user_image', 'T User Image:') !!}
    {!! Form::text('t_user_image', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('testimonials.index') !!}" class="btn btn-default">Cancel</a>
</div>
