<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $testimonial->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $testimonial->name !!}</p>
</div>

<!-- Testimonial Field -->
<div class="form-group">
    {!! Form::label('testimonial', 'Testimonial:') !!}
    <p>{!! $testimonial->testimonial !!}</p>
</div>

<!-- T User Image Field -->
<div class="form-group">
    {!! Form::label('t_user_image', 'T User Image:') !!}
    <p>{!! $testimonial->t_user_image !!}</p>
</div>

