<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $partner->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $partner->name !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $partner->url !!}</p>
</div>

<!-- Partner Image Field -->
<div class="form-group">
    {!! Form::label('partner_image', 'Partner Image:') !!}
    <p>{!! $partner->partner_image !!}</p>
</div>

