@extends("app")

@section('head_title', 'Agents | '.getcong('site_name') )
@section('head_url', Request::url())

@section("content")
<div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">
                  <div class="col-md-9">
                      <div class="block-heading">
                          <a href="register.html" class="btn btn-sm btn-primary pull-right">Become an agent <i class="fa fa-long-arrow-right"></i></a>
                          <h4><span class="heading-icon"><i class="fa fa-caret-right icon-design"></i><i class="fa fa-users"></i></span>All Agents</h4>
                      </div>
                    <div class="agents-listing">
                        <ul>
                           @foreach($agents as $i => $agent) 
                       <li class="col-md-12">
                              <div class="col-md-4">
                                      <a href="agent-detail.html" class="agent-featured-image"><div class="overlay" style="line-height:151px"><i class="fa fa-plus"></i></div>  <img src="{{ URL::asset('upload/members/'.$agent->image_icon.'-b.jpg') }}" alt="{{ $agent->name }}"></a>
                              </div>
                              <div class="col-md-8">
                                      <div class="agent-info">
                                      <div class="counts"><strong>18</strong><span>Properties</span></div>
                                      <h3><a href="agent-detail.html"{{$agent->name}}</a></h3>
                                      <p>{{$agent->about}}</p>
                                  </div>
                                  <div class="agent-contacts clearfix">
                                          <ul>
                                          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                          <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                                      </ul>
                                  </div>
                                  </div>
                              </li>
                   @endforeach 
                   
                       
                       
                        </ul>
                    </div>
                    <ul class="pagination">
                      <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                  </div>
                  <!-- Start Sidebar -->
                  <div class="sidebar right-sidebar col-md-3">
                       @include('_particles.search')
                        @include('_particles.featured')
                  
                  </div>   
              </div>
          </div>
      </div>
  </div>


    <div id="content">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="blog-container">
              <div class="blog-content">
                  
                <div class="the-team">
                  <div class="row container-realestate">
                   @foreach($agents as $i => $agent) 
                    <div class="col-md-4">
                      <div class="team-container team-dark">
                        <div class="team-image">
                          <img src="{{ URL::asset('upload/members/'.$agent->image_icon.'-b.jpg') }}" alt="{{ $agent->name }}">
                        </div>
                        <div class="team-description">
                          <h3>{{$agent->name}}</h3>
                          <p>{{$agent->about}}</p>
                          <div class="team-social">
                            <span><a href="{{$agent->twitter}}" title="Twitter" rel="tooltip" data-placement="top"><i class="fa fa-twitter"></i></a></span>
                            <span><a href="{{$agent->facebook}}" title="Facebook" rel="tooltip" data-placement="top"><i class="fa fa-facebook"></i></a></span>
                            <span><a href="{{$agent->gplus}}" title="Google Plus" rel="tooltip" data-placement="top"><i class="fa fa-google-plus"></i></a></span>
                            <span><a href="{{$agent->linkedin}}" title="LinkedIn" rel="tooltip" data-placement="top"><i class="fa fa-linkedin"></i></a></span>
                             
                          </div>                       
                        </div>
                      </div>
                    </div>
                    <!-- break -->
                   @endforeach 
                    
                  </div>
                  
                  @include('_particles.pagination', ['paginator' => $agents]) 
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end:content -->
 
@endsection
