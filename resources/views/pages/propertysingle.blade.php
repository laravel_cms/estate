@extends("app")

@section('head_title', $property->property_name .' | '.getcong('site_name') )
@section('head_description', substr(strip_tags($property->description),0,200))
@section('head_image', asset('/upload/properties/'.$property->featured_image.'-b.jpg'))
@section('head_url', Request::url())

@section("content")
<div class="main" role="main">
    <div id="content" class="content full">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="single-property">
              <h2 class="page-title">{{$property->property_name}}, <span class="location"><i class="fa fa-map-marker"></i> New York</span></h2>
              @if($property->property_purpose == 'Rent')
              <div class="price"><strong>$</strong><span>{{$property->rent_price}} Monthly</span></div>
              @else
              <div class="price"><strong>$</strong><span>{{$property->sale_price}} </span></div>
              @endif
              <div class="property-amenities clearfix"> 
                <span class="area"><strong>For</strong>
              {{$property->property_purpose}}</span>
               <span class="area">
                <strong> {{$property->area}}</strong>Area</span>
                 <span class="baths"><strong>{{$property->bathrooms}}</strong>Baths</span>
                  <span class="beds"><strong>{{$property->bedrooms}}</strong>Beds</span>
                   <span class="parking">
                  <strong>{{$property->parking}}</strong>Parking</span> </div>
              <div class="property-slider">
                <div id="property-images" class="flexslider">
                  
                            
                <div class="flex-viewport" style="overflow: hidden; position: relative;">
                  <ul class="slides" style="width: 800%; transition-duration: 0.6s; transform: translate3d(-2340px, 0px, 0px);">
                    <li class="item" style="width: 780px; float: left; display: block;"> <img src="{{ URL::asset('upload/properties/'.$property->featured_image.'-b.jpg') }}" alt=""> </li>
                    <li class="item" style="width: 780px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                    <li class="item" style="width: 780px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                    <li class="item flex-active-slide" style="width: 780px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                  </ul></div><ul class="flex-direction-nav"><li><a class="flex-prev" href="#"></a></li><li><a class="flex-next flex-disabled" href="#"></a></li></ul></div>
                <div id="property-thumbs" class="flexslider">
                  
                <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 800%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                    <li class="item" style="width: 175px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                    <li class="item" style="width: 175px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                    <li class="item" style="width: 175px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                    <li class="item flex-active-slide" style="width: 175px; float: left; display: block;"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </li>
                  </ul></div><ul class="flex-direction-nav"><li><a class="flex-prev flex-disabled" href="#"></a></li><li><a class="flex-next flex-disabled" href="#"></a></li></ul></div>
              </div>
              <div class="tabs">
                <ul class="nav nav-tabs">
                  <li class="active"><i class="fa fa-caret-down"></i>  <a data-toggle="tab" href="#description"> Description </a> </li>
                  <li><i class="fa fa-caret-down"></i>  <a data-toggle="tab" href="#amenities"> Additional Amenities </a> </li>
                </ul>
                <div class="tab-content">
                  <div id="description" class="tab-pane active">
                     {!!$property->description!!}
                  </div>
                  <div id="amenities" class="tab-pane">
                      <div class="additional-amenities">
                        <span class="available"><i class="fa fa-check-square"></i> Air Conditioning</span>
                         <span class="available"><i class="fa fa-check-square"></i> Heating</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Balcony</span>
                         <span class="available"><i class="fa fa-check-square"></i> Dishwasher</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Pool</span>
                         <span class="available"><i class="fa fa-check-square"></i> Internet</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Terrace</span>
                         <span class="available"><i class="fa fa-check-square"></i> Microwave</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Fridge</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Cable TV</span>
                         <span class="available"><i class="fa fa-check-square"></i> Security Camera</span>
                         <span class="available"><i class="fa fa-check-square"></i> Toaster</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Grill</span>
                         <span class="navailable"><i class="fa fa-check-square"></i> Oven</span>
                         <span class="available"><i class="fa fa-check-square"></i> Fans</span>
                      </div>
                  </div>
                </div>
              </div>
              <h3>Agent</h3>
              <div class="agent">
                  <div class="row">
                      <div class="col-md-4">
                          <img src="{{ URL::asset('upload/members/'.$agent->image_icon.'-b.jpg') }}" alt="Mia Kennedy">
                      </div>
                      <div class="col-md-8">
                        <h4><a href="agent-single.html">{{$agent->name}}</a></h4>

                         <p>{{$agent->about}}</p>
                          <div class="agent-contacts clearfix">
                            <a href="#" class="btn btn-primary pull-right btn-sm">Contact Agent</a>
                            <ul>
                              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                  <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <!-- Start Related Properties -->
            <h3>Related Properties</h3>
            <div class="property-grid">
              <ul class="grid-holder col-3 isotope" style="position: relative; overflow: hidden; height: 350px;">
                <li class="grid-item type-rent isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate(0px, 0px);">
                  <div class="property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:160px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Rent</span> </a>
                    <div class="property-info">
                      <h4><a href="#">116 Waverly Place</a></h4>
                      <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                      <div class="price"><strong>$</strong><span>2800 Monthly</span></div>
                    </div>
                    <div class="property-amenities clearfix"> <span class="area"><strong>5000</strong>Area</span> <span class="baths"><strong>3</strong>Baths</span> <span class="beds"><strong>3</strong>Beds</span> <span class="parking"><strong>1</strong>Parking</span> </div>
                  </div>
                </li>
                <li class="grid-item type-buy isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate(267px, 0px);">
                  <div class="property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:160px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Buy</span> </a>
                    <div class="property-info">
                      <h4><a href="#">232 East 63rd Street</a></h4>
                      <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                      <div class="price"><strong>$</strong><span>250000</span></div>
                    </div>
                    <div class="property-amenities clearfix"> <span class="area"><strong>5000</strong>Area</span> <span class="baths"><strong>3</strong>Baths</span> <span class="beds"><strong>3</strong>Beds</span> <span class="parking"><strong>1</strong>Parking</span> </div>
                  </div>
                </li>
                <li class="grid-item type-rent isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate(533px, 0px);">
                  <div class="property-block"> <a href="#" class="property-featured-image"><div class="overlay" style="line-height:160px"><i class="fa fa-search"></i></div> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Buy</span> </a>
                    <div class="property-info">
                      <h4><a href="#">55 Warren Street</a></h4>
                      <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                      <div class="price"><strong>$</strong><span>300000</span></div>
                    </div>
                    <div class="property-amenities clearfix"> <span class="area"><strong>5000</strong>Area</span> <span class="baths"><strong>3</strong>Baths</span> <span class="beds"><strong>3</strong>Beds</span> <span class="parking"><strong>1</strong>Parking</span> </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <!-- Start Sidebar -->
          <div class="sidebar right-sidebar col-md-3">
            @include('_particles.search')
             @include('_particles.featured')

          </div>
        </div>
      </div>
    </div>
  </div>


     @if (count($errors) > 0 or Session::has('flash_message'))
     <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

     <script type="text/javascript">    
	    $(window).load(function(){
	        $('#modal-error').modal('show');
	    });
	</script>
 	@endif
    <!-- begin:modal-message -->
    <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="modal-signin" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header" style="border-bottom:none;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           
          </div>
          <div class="modal-body">
               @if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
				@if(Session::has('flash_message'))
				    <div class="alert alert-success">
				     
				        {{ Session::get('flash_message') }}
				    </div>
				@endif
          </div>
          
        </div>
      </div>
    </div>
    <!-- end:modal-message -->
 
@endsection
