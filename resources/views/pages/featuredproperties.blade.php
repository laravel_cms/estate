@extends("app")

@section('head_title', 'Featured Properties | '.getcong('site_name') )
@section('head_url', Request::url())

@section("content")

 <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">
                  <div class="col-md-9">
                      <div class="block-heading">
                          <h4><span class="heading-icon"><i class="fa fa-home"></i></span>Featured Properties</h4>
                      </div>
                    <div class="property-grid">
                      <ul class="grid-holder col-3">
                         @foreach($properties as $i => $property) 
                         <li class="grid-item type-rent">
                          <div class="property-block"> <a href="#" class="property-featured-image">
                             <img src="{{ URL::asset('upload/properties/'.$property->featured_image.'-s.jpg') }}" alt="{{ $property->property_name }}"> 
                            <span class="images-count"><i class="fa fa-picture-o"></i> 2</span> <span class="badges">Rent</span> </a>
                            <div class="property-info">
                              <h4><a href="#">{{$property->property_name}}</a></h4>
                              <span class="location">NYC</span>
                              <div class="price"><strong>$</strong><span>{{$property->rent_price}} Monthly</span></div>
                            </div>
                            <div class="property-amenities clearfix"> <span class="area"><strong>{{$property->area}}</strong>Area</span> <span class="baths"><strong>{{$property->bathrooms}}</strong>Baths</span> <span class="beds"><strong>{{$property->bedrooms}}</strong>Beds</span> <span class="parking"><strong>{{$property->parking}}</strong>Parking</span> </div>
                          </div>
                        </li>
              <!-- break -->
              @endforeach
                      </ul>
                    </div>
                    <ul class="pagination">
                      <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                  </div>
                  <!-- Start Sidebar -->
                  <div class="sidebar right-sidebar col-md-3">
                       @include('_particles.search')
                        @include('_particles.featured')
                  
                  </div>  
              </div>
          </div>
      </div>
  </div>
 
 
@endsection
