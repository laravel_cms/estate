@extends("app")

@section('head_title', 'Properties | '.getcong('site_name') )
@section('head_url', Request::url())

@section("content")

 <div class="main" role="main">
      <div id="content" class="content full">
          <div class="container">
              <div class="row">
                  <div class="col-md-9">
                      <div class="block-heading">
                          <h4><span class="heading-icon"><i class="fa fa-caret-right icon-design"></i><i class="fa fa-th-list"></i></span>Property Listing</h4>
                          <div class="toggle-view pull-right">
                              <a href="grid-map.html"><i class="fa fa-th-large"></i></a>
                              <a href="/" class="active"><i class="fa fa-th-list"></i></a>
                          </div>
                      </div>
                    <div class="property-listing">
                        <ul>
                            @foreach($properties as $i => $property) 

                          <li class="type-rent col-md-12">
                            <div class="col-md-4"> <a href="#" class="property-featured-image">
                              <div class="overlay" style="line-height:151px"><i class="fa fa-search"></i></div>
                               <img src="{{ URL::asset('upload/properties/'.$property->featured_image.'-s.jpg') }}" alt="{{ $property->property_name }}"> 
                              <span class="images-count"><i class="fa fa-picture-o"></i> 2</span>
                               <span class="badges">{{ $property->property_type }}</span> </a> 
                             </div>
                            <div class="col-md-8">
                              <div class="property-info">
                                <div class="price"><strong>$</strong><span>
                                  @if($property->sale_price) {{$property->sale_price}} @else {{$property->rent_price}} Monthly @endif
                              </span></div>
                                <h3><a href="#">{{$property->property_name}}</a></h3>
                                <span class="location"><i class="fa fa-map-marker"></i> NYC</span>
                                <p> {!!$property->description !!} </p>
                              </div>
                              <div class="property-amenities clearfix"> <span class="area"><strong>{{$property->area}}</strong>Area</span> <span class="baths"><strong>{{$property->bathrooms}}</strong>Baths</span> <span class="beds"><strong>{{$property->bedrooms}}</strong>Beds</span> <span class="parking"><strong>{{$property->parking}}</strong>Parking</span> </div>
                            </div>
                          </li>
              <!-- break -->
              @endforeach


                        </ul>
                    </div>
                           
                  </div>
                  <!-- Start Sidebar -->
                  <div class="sidebar right-sidebar col-md-3">
                       @include('_particles.search')
                        @include('_particles.featured')
                  
                  </div>  
              </div>
          </div>
      </div>
  </div>


 
@endsection
