<?php
use App\Models\Setting;
use App\User;
use App\Models\Property;

 
if (! function_exists('getcong')) {

    function getcong($key)
    {
    	 
        $Setting = Setting::findOrFail('1'); 

        return $Setting->$key;
    }
}
 
if (!function_exists('classActivePath')) {
    function classActivePath($path)
    {
        $path = explode('.', $path);
        $segment = 2;
        foreach($path as $p) {
            if((request()->segment($segment) == $p) == false) {
                return '';
            }
            $segment++;
        }
        return ' active';
    }
}

if (!function_exists('classActivePathPublic')) {
    function classActivePathPublic($path)
    {
        $path = explode('.', $path);
        $segment = 1;
        foreach($path as $p) {
            if((request()->segment($segment) == $p) == false) {
                return '';
            }
            $segment++;
        }
        return ' active';
    }
}

if (! function_exists('getUserInfo')) {
	function getUserInfo($id) 
	{ 
		return User::find($id);
	}
}

if (! function_exists('countPropertyType')) {
	function countPropertyType($type) 
	{ 
		return Property::where('property_type',$type)->count();
	}
}
