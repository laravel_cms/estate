<?php

namespace App\Repositories;

use App\Models\Property;
use App\Repositories\BaseRepository;

/**
 * Class PropertyRepository
 * @package App\Repositories
 * @version August 5, 2019, 11:30 pm UTC
*/

class PropertyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'featured_property',
        'property_name',
        'property_slug',
        'property_type',
        'property_purpose',
        'sale_price',
        'rent_price',
        'address',
        'map_latitude',
        'map_longitude',
        'bathrooms',
        'bedrooms',
        'area',
        'description',
        'featured_image',
        'property_images1',
        'property_images2',
        'property_images3',
        'property_images4',
        'property_images5',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Property::class;
    }
}
