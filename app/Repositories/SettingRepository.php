<?php

namespace App\Repositories;

use App\Models\Setting;
use App\Repositories\BaseRepository;

/**
 * Class SettingRepository
 * @package App\Repositories
 * @version August 7, 2019, 8:14 pm UTC
*/

class SettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'site_style',
        'site_name',
        'site_email',
        'site_logo',
        'site_favicon',
        'site_description',
        'site_header_code',
        'site_footer_code',
        'site_copyright',
        'footer_widget1',
        'footer_widget2',
        'footer_widget3',
        'addthis_share_code',
        'disqus_comment_code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Setting::class;
    }
}
