<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Partner
 * @package App\Models
 * @version August 7, 2019, 8:14 pm UTC
 *
 * @property string name
 * @property string url
 * @property string partner_image
 */
class Partner extends Model
{

    public $table = 'partners';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'url',
        'partner_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'url' => 'string',
        'partner_image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'name' => 'required',
        'url' => 'required',
        'partner_image' => 'required'
    ];

    
}
