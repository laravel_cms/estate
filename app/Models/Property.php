<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Property
 * @package App\Models
 * @version August 5, 2019, 11:30 pm UTC
 *
 * @property integer user_id
 * @property integer featured_property
 * @property string property_name
 * @property string property_slug
 * @property string property_type
 * @property string property_purpose
 * @property string sale_price
 * @property string rent_price
 * @property string address
 * @property string map_latitude
 * @property string map_longitude
 * @property string bathrooms
 * @property string bedrooms
 * @property string area
 * @property string description
 * @property string featured_image
 * @property string property_images1
 * @property string property_images2
 * @property string property_images3
 * @property string property_images4
 * @property string property_images5
 * @property integer status
 */
class Property extends Model
{

    public $table = 'properties';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'featured_property',
        'property_name',
        'property_slug',
        'property_type',
        'property_purpose',
        'sale_price',
        'rent_price',
        'address',
        'map_latitude',
        'map_longitude',
        'bathrooms',
        'bedrooms',
        'area',
        'description',
        'featured_image',
        'property_images1',
        'property_images2',
        'property_images3',
        'property_images4',
        'property_images5',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'featured_property' => 'integer',
        'property_name' => 'string',
        'property_slug' => 'string',
        'property_type' => 'string',
        'property_purpose' => 'string',
        'sale_price' => 'string',
        'rent_price' => 'string',
        'address' => 'string',
        'map_latitude' => 'string',
        'map_longitude' => 'string',
        'bathrooms' => 'string',
        'bedrooms' => 'string',
        'area' => 'string',
        'description' => 'string',
        'featured_image' => 'string',
        'property_images1' => 'string',
        'property_images2' => 'string',
        'property_images3' => 'string',
        'property_images4' => 'string',
        'property_images5' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'user_id' => 'required',
        'featured_property' => 'required',
        'property_name' => 'required',
        'property_slug' => 'required',
        'property_type' => 'required',
        'property_purpose' => 'required',
        'sale_price' => 'required',
        'rent_price' => 'required',
        'address' => 'required',
        'map_latitude' => 'required',
        'map_longitude' => 'required',
        'bathrooms' => 'required',
        'bedrooms' => 'required',
        'area' => 'required',
        'description' => 'required',
        'featured_image' => 'required',
        'property_images1' => 'required',
        'property_images2' => 'required',
        'property_images3' => 'required',
        'property_images4' => 'required',
        'property_images5' => 'required',
        'status' => 'required',
        'created_at' => 'required',
        'updated_at' => 'required'
    ];

    
}
