<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Testimonial
 * @package App\Models
 * @version August 7, 2019, 8:26 pm UTC
 *
 * @property string name
 * @property string testimonial
 * @property string t_user_image
 */
class Testimonial extends Model
{

    public $table = 'testimonials';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'testimonial',
        't_user_image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'testimonial' => 'string',
        't_user_image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'name' => 'required',
        'testimonial' => 'required',
        't_user_image' => 'required'
    ];

    
}
