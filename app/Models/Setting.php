<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Setting
 * @package App\Models
 * @version August 7, 2019, 8:14 pm UTC
 *
 * @property string site_style
 * @property string site_name
 * @property string site_email
 * @property string site_logo
 * @property string site_favicon
 * @property string site_description
 * @property string site_header_code
 * @property string site_footer_code
 * @property string site_copyright
 * @property string footer_widget1
 * @property string footer_widget2
 * @property string footer_widget3
 * @property string addthis_share_code
 * @property string disqus_comment_code
 */
class Setting extends Model
{

    public $table = 'settings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'site_style',
        'site_name',
        'site_email',
        'site_logo',
        'site_favicon',
        'site_description',
        'site_header_code',
        'site_footer_code',
        'site_copyright',
        'footer_widget1',
        'footer_widget2',
        'footer_widget3',
        'addthis_share_code',
        'disqus_comment_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'site_style' => 'string',
        'site_name' => 'string',
        'site_email' => 'string',
        'site_logo' => 'string',
        'site_favicon' => 'string',
        'site_description' => 'string',
        'site_header_code' => 'string',
        'site_footer_code' => 'string',
        'site_copyright' => 'string',
        'footer_widget1' => 'string',
        'footer_widget2' => 'string',
        'footer_widget3' => 'string',
        'addthis_share_code' => 'string',
        'disqus_comment_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'site_style' => 'required',
        'site_name' => 'required',
        'site_email' => 'required',
        'site_logo' => 'required',
        'site_favicon' => 'required',
        'site_description' => 'required',
        'site_header_code' => 'required',
        'site_footer_code' => 'required',
        'site_copyright' => 'required',
        'footer_widget1' => 'required',
        'footer_widget2' => 'required',
        'footer_widget3' => 'required',
        'addthis_share_code' => 'required',
        'disqus_comment_code' => 'required'
    ];

    
}
