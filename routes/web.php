<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

	Route::resource('properties', 'PropertyController');

	Route::resource('settings', 'SettingController');

	Route::resource('partners', 'PartnerController');
	Route::resource('testimonials', 'TestimonialController');
});
Route::get('/home', 'HomeController@index');

Route::get('/', 'IndexController@index');

Route::post('subscribe', 'IndexController@subscribe');

Route::get('agents', 'AgentsController@index');

Route::get('properties', 'PropertiesController@index');

Route::get('featured', 'PropertiesController@featuredproperties');

Route::get('sale', 'PropertiesController@saleproperties');

Route::get('rent', 'PropertiesController@rentproperties');

Route::get('properties/{slug}', 'PropertiesController@propertysingle');

Route::get('type/{slug}', 'PropertiesController@propertiesbytype');

Route::post('agentscontact', 'PropertiesController@agentscontact');

Route::post('searchproperties', 'PropertiesController@searchproperties')->name('searchproperties');

Route::post('search', 'PropertiesController@searchkeywordproperties');



